define nginx::proxy (
   $site_name       = $title, 
   $proxy_name      = 'mytrax',
   $upstream_server = '192.168.2.129:8081',
   $ssl_certificate = '/etc/ssl/host.cert',
   $ssl_key         = '/etc/ssl/host.key',
   $is_enabled      = true) {

  $available = "${nginx::config_dir}/sites-available/$site_name"
  $enabled = "${nginx::config_dir}/sites-enabled/$site_name"
  
  File {
    owner => 'root',
    group => 'root',
  }    

  file { "$available":
    ensure => present,
    mode => '644',
    content   =>  template("nginx/ssl-proxy.erb"),
    require => Package["nginx"],
  }

  # Make a symlink in sites-enabled if the site should be turned on
  if $is_enabled {
    file { "$enabled":
      ensure  => link,
      target  => "$available",
      require => File["$available"],
      notify  => Service['nginx'],
    }
  }
  else {
    file { "$enabled":
      ensure => absent,
      notify  => Service['nginx'],
    }
  }
  
}