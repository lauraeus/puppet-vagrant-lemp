# Class: nginx
class nginx (

    $package_version = $nginx::params::nx_package_version, 
    $worker_processes = $nginx::params::nx_worker_processes, 
    $worker_rlimit_nofile  = $nginx::params::nx_worker_rlimit_nofile, 
  
    $sendfile  = $nginx::params::nx_sendfile, 
    $client_max_body_size  = $nginx::params::nx_client_max_body_size, 
    $client_header_buffer_size  = $nginx::params::nx_client_header_buffer_size, 
    $server_names_hash_bucket_size  = $nginx::params::nx_server_names_hash_bucket_size, 
    $server_names_hash_max_size  = $nginx::params::nx_server_names_hash_max_size, 
  
  )  inherits nginx::params  {
  
  $config_dir = "/etc/nginx"
  
  #All file resource declarations should be executed as root:root
  File {
    owner   =>  'root',
    group   =>  'root',
  }

  package { 'apache2': ensure => absent, }

  $noApacheMods = [ 'apache2.2-common', 'apache2.2-bin', 'apache2-utils', 'libapache2-mod-php5' ]
  package { $noApacheMods:
    ensure => absent,
    require => Package ['apache2'] ,  
  }

  # Installera NginX, ta bort eventuell Apache!
  package { 'nginx':
    ensure => "${package_version}", 
    require => Package [$noApacheMods] ,
  }

  # create a directory for wp nginx vhost directives
  file { '/etc/nginx/wordpress':
    ensure => "directory",
    require => Package['nginx'],
  }

  # create a directory for nginx microcache
  file { '/etc/nginx/microcache':
    ensure => 'directory',
    owner => 'www-data',
    require => Package['nginx'],
  }

  file { '/etc/nginx/wordpress/wpsecure.conf':
    source => "/etc/puppet/modules/nginx/files/wordpress/wpsecure.conf",
    notify => Service['nginx'],
    require => File['/etc/nginx/wordpress'],
  }

  file { '/etc/nginx/wordpress/wpredis.conf':
    source => "/etc/puppet/modules/nginx/files/wordpress/wpredis.conf",
    notify => Service['nginx'],
    require => File['/etc/nginx/wordpress'],
  }

  file { '/etc/nginx/wordpress/phpmyadmin.conf':
    source => "/etc/puppet/modules/nginx/files/wordpress/phpmyadmin.conf",
    notify => Service['nginx'],
    require => File['/etc/nginx/wordpress'],
  }

  file { '/etc/nginx/blacklist.conf':
    source => "/etc/puppet/modules/nginx/files/blacklist.conf",
    notify => Service['nginx'],
    require => Package['nginx'],
  }

  file { '/etc/nginx/php5-fpm.conf':
    source => "/etc/puppet/modules/nginx/files/php5-fpm.conf",
    notify => Service['nginx'],
    require => Package['nginx'],
  }

  file { "/etc/nginx/sites-enabled/default":
    notify => Service["nginx"],
    ensure => absent,
    require => Package["nginx"],
  }

  file { "/etc/nginx/nginx.conf":
    content   =>  template("/etc/puppet/modules/nginx/templates/nginx.conf.erb"),
    notify => Service["nginx"],
    require => Package["nginx"],
  }

  service { 'nginx':
    ensure => running,
    enable     => true,
    hasstatus => true,
    hasrestart => true,    
    require => Package['nginx'],
  }

  Service ['nginx'] {
    restart => 'service nginx configtest && service nginx restart',
  }

}
