define nginx::site (
   $site_name       = $title, 
   $is_enabled      = true) {

  $available = "${nginx::config_dir}/sites-available/$site_name"
  $enabled = "${nginx::config_dir}/sites-enabled/$site_name"

  File {
    owner => 'root',
    group => 'root',
  }    

  file { "$available":
    ensure => present,
    mode => '644',
    source => "/etc/puppet/modules/nginx/files/${fqdn}/${site_name}.conf",
    require => Package ['nginx'],
  }

  # Make a symlink in sites-enabled if the site should be turned on
  if $is_enabled {
    file { "$enabled":
      ensure  => link,
      target  => "$available",
      require => File["$available"],
      notify  => Service['nginx'],
    }
  }
  else {
    file { "$enabled":
      ensure => absent,
      notify  => Service['nginx'],
    }
  }
  
}