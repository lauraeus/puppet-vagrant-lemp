class nginx::params {

  $nx_package_version  = '1.4.4-4~precise0'
  
  ## root

  # reflect number of cpu cores
  $nx_worker_processes        = 8
  $nx_worker_rlimit_nofile    = 1024

  ## http

  # Use sendfile() syscall to speed up I/O operations 
  $nx_sendfile                = 'on'

  # Body size.
  $nx_client_max_body_size    = '10m'

  # GET parameter Facebook requires 5k ...
  $nx_client_header_buffer_size = '5k'

  # If a large number of server names are defined, or unusually long server names are defined,
  $nx_server_names_hash_bucket_size = '128'
  $nx_server_names_hash_max_size = '1024'

}
