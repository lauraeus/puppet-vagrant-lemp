# = Class: php
#
# This is the main php class

class redis {

 # redis-server för snabb RAM cachning av Wordpress
  if defined(Package['redis-server']) != true {
    package {'redis-server':     
      ensure => latest,   
    }  
  }

  if defined(Package['php5-redis']) != true {
    package {'php5-redis':     
      require => Package["redis-server"],
      ensure => latest,
    }  
  }

  service { 'redis-server':
    ensure => running,
    enable     => true,
    hasstatus => true,
    hasrestart => true,
    require => Package['redis-server'],
  }

}
