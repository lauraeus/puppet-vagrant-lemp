# Class: Git
class git () {
	include apt

	apt::ppa { 'ppa:git-core/ppa': 
	}

	apt::key { 'ppa:git-core/ppa': 
	    key    => 'E1DF1F24',
	}

	package { 'git-core': 
	  ensure => latest, 
	  require => [
	              Apt::Ppa['ppa:git-core/ppa'],
	              Apt::Key['ppa:git-core/ppa'],
	              ],
	}

}