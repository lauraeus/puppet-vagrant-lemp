# = Class: php
#
# This is the main php class

class php {

  ### Managed resources
  package { 'php5':
    ensure => latest,
  }

}
