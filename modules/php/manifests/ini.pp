# = Define: php::ini
#
define php::ini (
  $date_timezone    = 'Europe/Stockholm',
  $post_max_size    = '61M',
  $upload_max_filesize = '61M',
  $php5_fpm_listen_socket = '/var/run/php5-fpm.sock',
  ) {

  ini_setting { "php date_timezone":
    ensure  => present,
    path    => '/etc/php5/fpm/php.ini',
    section => 'Date',
    setting => 'date.timezone',
    value   => "${date_timezone}",
    require => Package['php5-fpm'],
    notify => Service['php5-fpm'],
  }

  ini_setting { "php post_max_size":
    ensure  => present,
    path    => '/etc/php5/fpm/php.ini',
    section => 'PHP',
    setting => 'post_max_size',
    value   => "${post_max_size}",
    require => Package['php5-fpm'],
    notify => Service['php5-fpm'],
  }

  ini_setting { "php upload_max_filesize":
    ensure  => present,
    path    => '/etc/php5/fpm/php.ini',
    section => 'PHP',
    setting => 'upload_max_filesize',
    value   => "${upload_max_filesize}",
    require => Package['php5-fpm'],
    notify => Service['php5-fpm'],
  }

  ini_setting { "php5-fpm unix socket":
    ensure  => present,
    path    => '/etc/php5/fpm/pool.d/www.conf',
    section => 'www',
    setting => 'listen',
    value   => "${php5_fpm_listen_socket}",
    require => Service['php5-fpm'],
  }

}
