# = Define: php::module
#
# Note that you may include or declare the php class when using
# the php::module define
#
define php::module (
  $version             = 'present',
  $service_autorestart = '',
  ) {

  include php

  if $absent {
    $real_version = 'absent'
  } else {
    $real_version = $version
  }

  if defined(Package[$name]) == false {
    package { "PhpModule_${name}":
      ensure  => $real_version,
      name    => "${name}",
      notify  => $real_service_autorestart,
      require => Package['php5'],
    }
  }

}
