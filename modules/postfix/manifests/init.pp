# = Class: postfix
#

class postfix (
  $postfix_myhostname           = 'server.lauraeus.se',
  $postfix_mydestination        = 'server.lauraeus.se, localhost.lauraeus.se, localhost',
  $postfix_relayhost            = 'smtp.bredband.net'
  ) {

  package { 'postfix':
    ensure => present,
  }

  service { 'postfix':
    ensure     => running,
    enable     => true,
    hasstatus  => true,
    require    => Package['postfix'],
    restart    => '/etc/init.d/postfix restart',
  }

  file { '/etc/postfix/main.cf':
    owner   => 'root',
    group   => 'root',
    require => Package['postfix'],
    notify  => Service ['postfix'],
    content => template("/etc/puppet/modules/postfix/templates/main.cf.erb"),
  }

}
