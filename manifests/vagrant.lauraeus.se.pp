#
# Usage:
#  git pull origin master && puppet apply manifests/vagrant.lauraeus.se.pp
#

# PATH
#Exec {
#  path => "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
#}
Exec { path => [ '/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/', '/usr/local/sbin', '/usr/local/bin' ] }

## Signallera 
notify { "Fil: manifests/vagrant.lauraeus.se.pp" : }
notify { "fqdn: ${fqdn}" : }

## Repos för senaste NginX, PHP5.5 etc
#===================================
class apt-repos {

  # uppdatera en ggr per vecka
  exec { 'apt-get update':
    command => '/usr/bin/apt-get update',
    onlyif => "/bin/bash -c 'exit $(( $(( $(date +%s) - $(stat -c %Y /var/lib/apt/lists/$( ls /var/lib/apt/lists/ -tr1|tail -1 )) )) <= v604800 ))'"
  }

  $aptDepPacks = [ 'build-essential', 'wget', 'python-software-properties' ]
  package { $aptDepPacks:
      ensure => latest,
      require => Exec [ 'apt-get update' ],
  }
  
  # Ensure apt-get update has been run before installing any packages
  Exec['apt-get update'] -> Package <| |>

  exec { "add-apt-repository ppa:nginx/stable && apt-get update":
      alias => "nginx_repository",
      require => Package["python-software-properties"],
      creates => '/etc/apt/sources.list.d/nginx-stable-natty.list',
  }

  exec { "add-apt-repository ppa:ondrej/php5 && apt-get update":
      alias => "php_repository",
      require => Package["python-software-properties"],
      creates => '/etc/apt/sources.list.d/php-ondrej-stable.list',
  }

  exec { "add-apt-repository ppa:git-core/ppa && apt-get update":
      alias => "git_core_repo",
      require => Package["python-software-properties"],
      creates => '/etc/apt/sources.list.d/git-core-stable.list',
  }

}

## Bra att ha småprogram
#===================================
class system-utils {
  
	$devPackages = [ 'nano', 'curl', 'htop', 'git-core' ]
	package { $devPackages:
	  ensure => latest,
	}

}

## NginX server
#===================================
class nginx-setup {   
  
  class { 'nginx':
    #package_version => '1.4.4-4~precise0',
    package_version => 'latest',
    sendfile => 'off',
    worker_processes => '1',
    client_max_body_size => '11m',
  } 

  nginx::site { 'nginx vhost wordpress':
    site_name       => 'wordpress',
    is_enabled      => true,
  }
}

## mysql för Wordpress
#===================================
class mysql-setup {
  class { "mysql::server":
      root_password => 'auto',
  }

  mysql::db { 'wordpress':
      user     => 'wordpress',
      password => 'wordpress-vagrant',
      host     => 'localhost',
      grant    => ['ALL'],
  }

}

## PHP 5.5 för Wordpress
#===================================
class php-setup {

  class { 'php': }

  php::module { "php5-cgi": version => 'latest', service_autorestart => 'php5-fpm' }
  php::module { "php5-cli": }

  php::module { "php5-gd": }
  php::module { "php5-curl": version => 'latest', service_autorestart => 'php5-fpm'}
  php::module { "php5-mcrypt": version => 'latest'}
  php::module { "php5-imagick": }
  php::module { "php5-intl": }
  php::module { "php5-dev": }

  php::module { "php5-mysql": version => 'latest', service_autorestart => 'php5-fpm' }

  php::module { "php5-fpm": version => 'latest'}

  ## inifiler
  #
  ini_setting { "php datumzonen sthlm":
    ensure  => present,
    path    => '/etc/php5/fpm/php.ini',
    section => 'Date',
    setting => 'date.timezone',
    value   => 'Europe/Stockholm',
    require => Package['php5-fpm'],
    notify => Service['php5-fpm'],
  }

  ini_setting { "php post_max_size":
    ensure  => present,
    path    => '/etc/php5/fpm/php.ini',
    section => 'PHP',
    setting => 'post_max_size',
    value   => '61M',
    require => Package['php5-fpm'],
    notify => Service['php5-fpm'],
  }

  ini_setting { "php upload_max_filesize":
    ensure  => present,
    path    => '/etc/php5/fpm/php.ini',
    section => 'PHP',
    setting => 'upload_max_filesize',
    value   => '61M',
    require => Package['php5-fpm'],
    notify => Service['php5-fpm'],
  }

  ini_setting { "php5-fpm unix socket":
    ensure  => present,
    path    => '/etc/php5/fpm/pool.d/www.conf',
    section => 'www',
    setting => 'listen',
    value   => '/var/run/php5-fpm.sock',
    require => Service['php5-fpm'],
  }

  ## Tjänst
  #
  service { 'php5-fpm':
    ensure => running,
    enable     => true,
    hasstatus => true,
    hasrestart => true,
    require => Php::Module['php5-fpm'],
  } 

}

## redis backend för Wordpress
#===================================
class redis-setup {
  include redis
}

## phpMyAdmin ger GUI för DB import för utveckling
#===================================
class phpmyadmin-setup {
  package { "phpmyadmin":
    ensure => present,
  }
}

## postfix för Wordpress mailout
#===================================
class postfix-setup {
  class { 'postfix':
    postfix_myhostname    => 'server.lauraeus.se',
    postfix_mydestination => 'server.lauraeus.se, localhost.lauraeus.se, localhost',
    postfix_relayhost    => 'smtp.google.com',
  }
}

# END Defs


#
## Inkludera alla resurser
#===================================
include apt-repos
include system-utils
include mysql-setup
include nginx-setup
include php-setup
include redis-setup
include phpmyadmin-setup 
include postfix-setup 

anchor { 'vps::begin':}
anchor { 'vps::end':}

#Installation order
Anchor['vps::begin']      ->
  Class['::apt-repos']    ->
  Class['::system-utils']  ->
  Class['::mysql-setup']   ->
  Class['::nginx-setup']  ->
  Class['::php-setup']  ->
  Class['::redis-setup']  ->
  Class['::phpmyadmin-setup']  ->
  Class['::postfix-setup']  ->
Anchor['vps::end']