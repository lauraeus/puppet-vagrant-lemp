 ## Git v1.7.10
  #=====================================

  include apt

  #Include git ppa (gitlab requires git 1.7.10 or newer which isn't in standard repo)
  apt::ppa { 'ppa:git-core/ppa':
  }

  #Install key for repo (otherwise it prints error)
  apt::key { 'ppa:git-core/ppa':
      key   =>  'E1DF1F24',
  }

  package { 'git-core':
    ensure  =>  present,
    require =>  [
        Apt::Ppa['ppa:git-core/ppa'],
        Apt::Key['ppa:git-core/ppa'],
                ],
  }